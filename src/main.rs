use std::collections::HashMap;
use std::io::{self, Read};
use std::path::PathBuf;

use clap::{App, Arg};

use chrono::Utc;

use log::{error, info};

fn main() {
    simple_logger::init().unwrap();
    // Pipe stuff
    let mut content = String::new();
    io::stdin()
        .lock()
        .read_to_string(&mut content)
        .ok()
        .unwrap();
    info!("Contents:{}", content);

    // Flag stuff
    info!("Reading flags");
    let matches = App::new("rsp")
        .arg(
            Arg::with_name("location")
                .short("l")
                .long("location")
                .multiple(true)
                .required(true)
                .help("Location of thingy in url form. http and file and currently supported")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("Form")
                .short("F")
                .long("Form")
                .help("Sets options for a form web request")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .help("Sets the form value for stdin")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .multiple(true)
                .possible_values(&["stdout", "clipboard", "primary", "None"])
                .help("Set the output of the save location. For now it prioritizes http > file")
                .takes_value(true),
        )
        .get_matches();
    info!("Making Task Manager");
    let mut tasks: Vec<Box<Save>> = std::vec::Vec::new();
    let mut locations = Vec::new();
    let mut form = HashMap::new();
    if matches.is_present("Form") {
        let forms_vec: Vec<&str> = matches.values_of("Form").unwrap().collect();
        for k in forms_vec {
            let mut iter = k.split('=');
            let (k, v) = (iter.next().unwrap(), iter.next().unwrap());
            form.insert(k, v);
        }
    }
    if let Some(input) = matches.value_of("input") {
        form.insert(input, content.as_ref());
    }
    if let Some(v) = matches.value_of("location") {
        locations.push(v);
    }
    info!("Reading from args and setting values");
    for v in locations {
        info!("{}", v);
        if v.starts_with("http://") {
            info!("http request inc");
            tasks.push(Box::new(WebRaw::new(v.to_owned(), form.clone())));
        } else if v.starts_with("file://") {
            info!("file request inc");
            let now = Utc::now(); //@TODO(renzix): Better file names?
            let v = &v[7..];
            if !std::path::Path::new(v).exists() {
                std::fs::create_dir_all(v).ok().unwrap(); //@TODO(renzix): Deal with a possible dir cant be created/permission error?
            }
            tasks.push(Box::new(LocalLoc::new(PathBuf::from(format!(
                "{}/{}",
                v.to_string(),
                now.timestamp_millis()
            )))));
        } else {
            panic!("Couldnt understand your url request please try again or read --help. We only accept http and file currently");
        }
    }
    if tasks.is_empty() {
        panic!("No tasks were given");
    }
    // Do each task which was added from config above
    for t in &mut tasks {
        t.save(&content);
    }

    // Any other final tasks
    if matches.is_present("output") {
        let output_vec: Vec<&str> = matches.values_of("output").unwrap().collect();
        for o in output_vec {
            match o {
                "clipboard" => {
                    tasks[0].save_to_clip(ClipType::Clipboard);
                }
                "primary" => {
                    tasks[0].save_to_clip(ClipType::Primary);
                }
                "stdout" => {
                    println!("{}", content);
                }
                _ => (),
            }
        }
    }
}

trait Save {
    fn save(&mut self, content: &String) -> bool;
    fn save_to_clip(&self, clip: ClipType) -> bool;
}

struct WebRaw<'a> {
    //@TODO(renzix): Add support for https
    url: String,
    form: HashMap<&'a str, &'a str>,
    res: Option<String>,
}

impl<'a> WebRaw<'a> {
    fn new(url: String, form: HashMap<&'a str, &'a str>) -> WebRaw<'a> {
        WebRaw {
            url,
            form,
            res: None,
        }
    }
    fn save_to_server(&mut self) -> bool {
        info!("Connecting to {}", self.url);
        let server = reqwest::Client::new();
        let res = match server.post(self.url.as_str()).form(&self.form).send() {
            Ok(mut s) => s.text().ok().unwrap(),
            Err(e) => {
                error!("Couldnt connect properly to the server, error: {:?}", e);
                return false;
            }
        };
        info!("REQUEST:{}", res);
        self.res = Some(res);
        true
    }
}

impl<'a> Save for WebRaw<'a> {
    fn save(&mut self, content: &String) -> bool {
        self.save_to_server() // as of rn this doesnt do anything with content
    }
    /// This is really fucking hacky because x11 doesnt save your clipboard
    fn save_to_clip(&self, clip: ClipType) -> bool {
        if let Some(res) = &self.res {
            use clipboard::x11_clipboard::{Clipboard, Primary, X11ClipboardContext};
            use clipboard::ClipboardProvider;
            match clip {
                ClipType::Primary => {
                    let mut ctx: X11ClipboardContext<Primary> = match ClipboardProvider::new() {
                        Ok(ctx) => ctx,
                        _ => return false,
                    };
                    let ret = ctx.set_contents(res.to_owned());
                    while { res.to_owned() == ctx.get_contents().unwrap_or("".to_string()) } {
                        std::thread::sleep(std::time::Duration::from_secs(5))
                    }
                    ret.is_ok()
                }
                ClipType::Clipboard => {
                    let mut ctx: X11ClipboardContext<Clipboard> = match ClipboardProvider::new() {
                        Ok(ctx) => ctx,
                        _ => return false,
                    };
                    let ret = ctx.set_contents(res.to_owned());
                    while { res.to_owned() == ctx.get_contents().unwrap_or("".to_string()) } {
                        std::thread::sleep(std::time::Duration::from_secs(5))
                    }
                    ret.is_ok()
                }
            }
        } else {
            false
        }
    }
}

struct LocalLoc {
    url: std::path::PathBuf,
    content: String,
}

impl LocalLoc {
    fn new(url: std::path::PathBuf) -> LocalLoc {
        LocalLoc {
            url,
            content: "".to_string(),
        }
    }
}

impl Save for LocalLoc {
    fn save(&mut self, content: &String) -> bool {
        use std::fs::File;
        use std::io::Write;
        self.content = content.to_string();
        info!("Saving content to file at {:#?}", self.url);
        let mut file = File::create(&self.url).expect(
            "Couldn't save the file. Make sure you have permission and the location exists.",
        );
        file.write_all(content.as_bytes()).is_ok()
    }
    /// This is really fucking hacky because x11 doesnt save your clipboard
    fn save_to_clip(&self, clip: ClipType) -> bool {
        use clipboard::x11_clipboard::{Clipboard, Primary, X11ClipboardContext};
        use clipboard::ClipboardProvider;
        match clip {
            ClipType::Primary => {
                let mut ctx: X11ClipboardContext<Primary> = match ClipboardProvider::new() {
                    Ok(ctx) => ctx,
                    _ => return false,
                };
                let ret = ctx.set_contents(self.content.to_owned());
                while self.content.to_owned() == ctx.get_contents().unwrap_or("".to_string()) {
                    std::thread::sleep(std::time::Duration::from_secs(5))
                }
                ret.is_ok()
            }
            ClipType::Clipboard => {
                let mut ctx: X11ClipboardContext<Clipboard> = match ClipboardProvider::new() {
                    Ok(ctx) => ctx,
                    _ => return false,
                };
                let ret = ctx.set_contents(self.content.to_owned());
                while self.content.to_owned() == ctx.get_contents().unwrap_or("".to_string()) {
                    std::thread::sleep(std::time::Duration::from_secs(5))
                }
                ret.is_ok()
            }
        }
    }
}

enum ClipType {
    Clipboard,
    Primary,
}
